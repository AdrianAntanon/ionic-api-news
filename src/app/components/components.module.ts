import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news/news.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { SingleNewsComponent } from './single-news/single-news.component';

@NgModule({
  declarations: [NewsComponent, HeaderComponent, SingleNewsComponent],
  imports: [CommonModule, IonicModule],
  exports: [NewsComponent, HeaderComponent, SingleNewsComponent],
})
export class ComponentsModule { }
