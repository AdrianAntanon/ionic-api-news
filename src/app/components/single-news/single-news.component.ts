import { Component, Input, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';

import { Article } from 'src/app/interfaces/interfaces';
import { LocalDataService } from 'src/app/services/local-data.service';

@Component({
  selector: 'app-single-news',
  templateUrl: './single-news.component.html',
  styleUrls: ['./single-news.component.scss'],
})

export class SingleNewsComponent implements OnInit {

  @Input() singleNews: Article;
  @Input() currentIndex: number;
  @Input() isFavorite: any;



  constructor(public actionSheetController: ActionSheetController, private localData: LocalDataService) { }

  ngOnInit() {
  }

  async openNew() {
    const { Browser } = Plugins;

    const newsUrl = this.singleNews.url;

    await Browser.open({ url: newsUrl });
  }

  async menuActivate() {
    let favButton: { text: string; icon: string; handler: (() => void) | (() => void); };

    if (this.isFavorite) {
      favButton = {
        text: 'Eliminar favorito',
        icon: 'trash',
        handler: () => {
          this.localData.deleteFavoriteNews(this.singleNews);
        }
      }
    } else {
      favButton = {
        text: 'Favorito',
        icon: 'star',
        handler: () => {
          this.localData.saveFavoriteNews(this.singleNews);
        }
      };
    }

    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Compartir',
        icon: 'share',
        handler: async () => {
          const { Share } = Plugins;
          // Comentar con Dani que cuando lo emulo en el móvil no se ven las noticias
          const shareRet = await Share.share({
            title: this.singleNews.title,
            text: this.singleNews.description,
            url: this.singleNews.url,
            dialogTitle: 'Compartir con conocidos'
          });

          shareRet();
        }
      },
        favButton,
      {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
  }

}
