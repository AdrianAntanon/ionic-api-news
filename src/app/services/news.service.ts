import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { NewsResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  mainPage = 0;
  BASE_URL = `https://newsapi.org/v2/top-headlines?language=es`;
  currentCategory = '';
  pageCategory = 0;

  constructor() { }

  async getNews() {
    this.getNextPage();

    const response: NewsResponse = await (
      await fetch(
        `${this.BASE_URL}&page=${this.mainPage}&apiKey=${environment.apiKey}`
      )
    ).json();


    return response;

  }

  async getNewsByCategory(category: string) {

    if (this.currentCategory === category) {
      this.pageCategory++;
    } else {
      this.pageCategory = 1;
      this.currentCategory = category;
    }

    const response: NewsResponse = await (
      await fetch(
        `${this.BASE_URL}&page=${this.pageCategory}&category=${category}&apiKey=${environment.apiKey}`
      )
    ).json();

    return response;
  }

  getNextPage() {
    this.mainPage++;
  }

}
