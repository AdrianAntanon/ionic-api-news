import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Article } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {
  newsOnDevice: Article[] = [];

  constructor(private storage: Storage) {
    this.storage.create();
    this.loadFavoriteNews();
  }

  saveFavoriteNews(news: Article) {

    const isSaved = this.newsOnDevice.find(singleNews => singleNews.title === news.title);
    if (!isSaved) {
      this.newsOnDevice.unshift(news);
      this.storage.set('favorites', this.newsOnDevice);
    }
  }

  deleteFavoriteNews(news: Article) {
    this.newsOnDevice = this.newsOnDevice.filter(singleNews => singleNews.title !== news.title);
    this.storage.set('favorites', this.newsOnDevice);

  }

  async loadFavoriteNews() {

    const favorites = await this.storage.get('favorites');


    if (favorites) {
      this.newsOnDevice = favorites;

      return this.newsOnDevice;
    }

  }

}
