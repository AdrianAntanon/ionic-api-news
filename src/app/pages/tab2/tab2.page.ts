import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  // @ViewChild(IonSegment) segment: IonSegment; //No sirve, me lo marca como undefined

  newsCategories = [
    'business',
    'entertainment',
    'general',
    'health',
    'science',
    'sports',
    'technology',
  ];
  mainCategory = 'business';

  articles: Article[] = [];

  constructor(private data: NewsService) { }

  ngOnInit(): void {

    this.loadNewsByCategory(this.newsCategories[0]);
  }

  segmentChanged(event: any) {
    this.mainCategory = event.detail.value;
    this.articles = [];
    this.loadNewsByCategory(event.detail.value.toLowerCase());
  }

  loadNewsByCategory(category: string, event?) {
    (async () => {
      const response = await this.data.getNewsByCategory(category);
      if (response.articles.length === 0) {
        event.target.disables = true;
        event.target.complete();
        return;
      }
      this.articles.push(...response.articles);
      if (event) {
        event.target.complete();
      }
    })();
  }

  loadData(event: any) {
    this.loadNewsByCategory(this.mainCategory, event);
    event.target.complete();
  }
}
